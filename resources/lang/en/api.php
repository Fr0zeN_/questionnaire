<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Api Messages Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during api response for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'questionnaire' => [
        'store_success' => 'Success: Questionnaire insert successful',
        'store_fail' => 'Error: Whoops! something went wrong',
        'store_questions_success' => 'Success: Questionnaire questions insert successful',
        'store_questions_fail' => 'Error: Whoops! something went wrong',
    ],

];
