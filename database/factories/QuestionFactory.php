<?php

use Faker\Generator as Faker;

$factory->define(App\Question::class, function (Faker $faker) {
    $type = \App\QuestionType::get()->random();
    return [
        'question'=>$faker->sentence,
//        'answer'=>$faker->randomElement([$faker->sentence,null,$faker->sentence,null]),
        'answer'=>$type->name == "Text" ? $this->faker->randomElement([$this->faker->sentence,$this->faker->sentence,$this->faker->sentence]) : null,
        'question_type_id'=>$type->id
    ];
});
