<?php

use Faker\Generator as Faker;

$factory->define(App\Questionnaire::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'duration' => $faker->randomElement(['45','30','15','60','120']),
        'duration_type' => $faker->randomElement(['minutes','hours']),
        'resumeable' => $faker->randomElement([0,1]),
        'published' => $faker->randomElement([0,1]),
        'user_id' => $faker->randomElement(\App\User::all()->pluck('id')->toArray())
    ];
});
