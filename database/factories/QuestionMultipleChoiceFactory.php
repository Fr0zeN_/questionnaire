<?php

use Faker\Generator as Faker;

$factory->define(App\QuestionMultipleChoice::class, function (Faker $faker) {
    return [
        'choice' => $faker->sentence,
        'correct' => $faker->randomElement([0,1])
    ];
});
