<?php

use Illuminate\Database\Seeder;

class QuestionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\QuestionType::create(['name'=>'text']);
        \App\QuestionType::create(['name'=>'mcqs']);
        \App\QuestionType::create(['name'=>'mcqm']);
    }
}
