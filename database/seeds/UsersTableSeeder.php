<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // How many genres you need, defaulting to 10
        $count = (int)$this->command->ask('How many users do you need ?', 10);

        $this->command->info("Creating {$count} Users.");

        // Create the Genre
        $users = factory(\App\User::class, $count)->create()->each(function($user){
            $this->command->info($user->email);

        });

        $this->command->info("{$count} Users Created.");
    }
}
