<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;


class QuestionnairesTableSeeder extends Seeder
{

    private $faker;

    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // How many genres you need, defaulting to 10
        $count = (int)$this->command->ask('How many questionnaires do you need ?', 10);

        $this->command->info("Creating {$count} questionnaires.");

        // Create the Genre
        $questionnaires = factory(\App\Questionnaire::class, $count)->create()->each(function($questionnaire){

            $questionnaire->questions()->saveMany(factory(\App\Question::class,rand(1,10))->create(['questionnaire_id'=>$questionnaire->id]))->each(function($question){
                if($question->questionType->name !=="text"){
                    $question->choices()->saveMany(factory(\App\QuestionMultipleChoice::class,rand(1,10)))->create(['question_id'=>$question->id]);
                }
            });

        });

        $this->command->info("{$count} Questionnaires Created.");


    }
}
