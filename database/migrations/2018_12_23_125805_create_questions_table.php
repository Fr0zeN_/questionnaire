<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('questionnaire_id')->unsigned();
            $table->foreign('questionnaire_id')
                ->references('id')
                ->on('questionnaires')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->integer('question_type_id')->unsigned();
            $table->foreign('question_type_id')
                ->references('id')
                ->on('question_types')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->string('question');
            $table->string('answer')->nullable();


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
