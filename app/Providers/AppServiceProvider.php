<?php

namespace App\Providers;

use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Collection::macro('recursive', function () {
            return $this->map( function ( $value ) {

                if ( is_array( $value ) || is_object( $value ) ) {
                    return collect( (object) $value )->recursive();
                }

                return $value;
            } );
        });
    }
}
