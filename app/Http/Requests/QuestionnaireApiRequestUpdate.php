<?php

namespace App\Http\Requests;

use App\Questionnaire;
use Illuminate\Foundation\Http\FormRequest;

class QuestionnaireApiRequestUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $questionnaire = Questionnaire::findOrFail($this->route('questionnaire'));

        return $questionnaire && $this->user('api')->can('update', $questionnaire);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
