<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionnaireApiRequestUpdate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class QuestionnaireApiController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $questionnaire = DB::table('questionnaires')
            ->where('questionnaires.user_id','=', $request->user('api')->id)
            ->select(['questionnaires.*', DB::raw('COUNT(questions.id) as questions_count'),DB::raw("CONCAT(questionnaires.duration,' ',questionnaires.duration_type) as duration")])
            ->selectRaw("IF(questionnaires.resumeable = 1, 'Yes', 'No') as resumeable")
            ->selectRaw("IF(questionnaires.published = 1, 'Yes', 'No') as published")
            ->groupBy('questionnaires.id')
            ->where('questionnaires.name','LIKE','%'.$request->filter.'%')
            ->leftjoin('questions', 'questions.questionnaire_id', '=', 'questionnaires.id')
            ->paginate(3)->toJson();

        return response($questionnaire);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'name' => $request->name,
            'duration' => $request->duration,
            'duration_type' => $request->duration_type,
            'resumeable' => $request->resumeable,
            'user_id' => $request->user('api')->id,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now(),
        ];

        try {
            $questionID = DB::table('questionnaires')->insertGetId($data);
            $question = DB::table('questionnaires')->where('id', $questionID)->select('id', 'name', 'duration', 'duration_type', 'resumeable')->first();
            if ($question) {
                return response()->json([
                    'api_status' => 1,
                    'api_message' => trans('api.questionnaire.store_success'),
                    'api_http' => 200,
                    'questionnaire' => [
                        'status' => true,
                        'message' => trans('api.questionnaire.store_success'),
                        'data' => $question
                    ]
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'api_status' => 1,
                'api_message' => trans('api.questionnaire.store_fail'),
                'api_http' => $e->getCode(),
                'questionnaire' => [
                    'status' => false,
                    'message' => trans('api.questionnaire.store_fail')
                ]
            ]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(QuestionnaireApiRequestUpdate $request, $id)
    {
        //recursive collection
        $questions = collect($request->all())->recursive();

        try {
            foreach ($questions as $question) {

                //get question type id
                $typeID = DB::table('question_types')->where('name', $question->get('type'))->select('id')->first()->id;

                //store basic question details including type text
                $questionID = DB::table('questions')->insertGetId(['questionnaire_id' => $id, 'question_type_id' => $typeID, 'question' => $question->get('question'), 'answer' => $question->get('answer'), 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);

                //store multiple type choices
                if ($question->get('type') !== 'text') {
                    foreach ($question->get('choices') as $choice) {
                        DB::table('question_multiple_choices')->insert(['question_id' => $questionID, 'choice' => $choice->get('choice'), 'correct' => $choice->get('correct') == true ? 1 : 0, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
                    }
                }
            }

            return response()->json([
                'api_status' => 1,
                'api_message' => trans('api.questionnaire.store_questions_success'),
                'api_http' => 200,
                'questionnaire' => [
                    'status' => true,
                    'message' => trans('api.questionnaire.store_questions_success'),
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'api_status' => 1,
                'api_message' => trans('api.questionnaire.store_questions_fail'),
                'api_http' => 200,
                'questionnaire' => [
                    'status' => false,
                    'message' => trans('api.questionnaire.store_questions_success'),
                ]
            ]);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
