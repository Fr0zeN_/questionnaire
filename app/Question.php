<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //

    public function questionType()
    {
        return $this->belongsTo(QuestionType::class);
    }

    public function choices()
    {
        return $this->hasMany(QuestionMultipleChoice::class);
    }

}
